package br.ufpr.ees008.android.restaurante.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import br.ufpr.ees008.android.restaurante.R;
import br.ufpr.ees008.android.restaurante.RestauranteApplication;
import br.ufpr.ees008.android.restaurante.adapters.OrderItemAdapter;
import br.ufpr.ees008.android.restaurante.preferences.AuthPreferences;
import br.ufpr.ees008.android.restaurante.service.OrderService;
import br.ufpr.ees008.android.restaurante.vo.AuthenticationVO;
import br.ufpr.ees008.android.restaurante.vo.ItemVO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderItemActivity extends AppCompatActivity {
    OrderService orderService;
    AuthPreferences authPreferences;
    ListView orderItemListView;
    AlertDialog alertDialog;
    Long orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);
        getSupportActionBar().setTitle("Novo pedido");
        orderService = RestauranteApplication.getOrderService();
        authPreferences = RestauranteApplication.getAuthPreferences();
        orderItemListView = (ListView) findViewById(R.id.itemListView);
        orderId = getIntent().getLongExtra("order_id", 0);
        orderItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, final long id) {
                final EditText editText = new EditText(OrderItemActivity.this);
                editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);

                alertDialog = new AlertDialog.Builder(OrderItemActivity.this)
                        .setView(editText)
                        .setCancelable(true)
                        .setTitle("Quantidade de items")
                        .setPositiveButton("Pedir", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String amountStr = editText.getText().toString();
                                if (!amountStr.isEmpty()) {
                                    int amount = Integer.valueOf(amountStr);
                                    if (amount > 0) {
                                        orderService.addItem(
                                                authPreferences.getToken(), id, amount, orderId)
                                                .enqueue(new Callback<AuthenticationVO>() {
                                                    @Override
                                                    public void onResponse(Call<AuthenticationVO> call, Response<AuthenticationVO> response) {
                                                        if (response.isSuccessful()) {
                                                            setResult(RESULT_OK);
                                                            finish();
                                                        } else {
                                                            if (response.code() == 401) {
                                                                authPreferences.logout();
                                                                showLoginScreen();
                                                                finish();
                                                            }
                                                            Toast.makeText(OrderItemActivity.this, "Tente novamente mais tarde.", Toast.LENGTH_LONG).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<AuthenticationVO> call, Throwable t) {
                                                        Toast.makeText(OrderItemActivity.this, "Tente novamente mais tarde.", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                    } else {
                                        Toast.makeText(OrderItemActivity.this, "A quantidade deve ser maior que 0.", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(OrderItemActivity.this, "Preencha o campo quantidade.", Toast.LENGTH_LONG).show();
                                }

                            }
                        })
                        .setNegativeButton("Cancelar", null)
                        .show();
            }
        });
        getItems();
    }

    @Override
    protected void onStop() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        super.onStop();
    }

    public void showLoginScreen() {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }

    public void getItems() {
        orderService.listItems(authPreferences.getToken()).enqueue(new Callback<List<ItemVO>>() {
            @Override
            public void onResponse(Call<List<ItemVO>> call, Response<List<ItemVO>> response) {
                if (response.isSuccessful()) {
                    orderItemListView.setAdapter(new OrderItemAdapter(OrderItemActivity.this, response.body()));
                }
            }

            @Override
            public void onFailure(Call<List<ItemVO>> call, Throwable t) {

            }
        });
    }
}
