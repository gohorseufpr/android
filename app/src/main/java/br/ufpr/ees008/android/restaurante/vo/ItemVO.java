package br.ufpr.ees008.android.restaurante.vo;


import android.util.Base64;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class ItemVO {
    Long id;
    String name;
    Long itemCategory;
    BigDecimal price;
    @SerializedName("picture_b64")
    String picture;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(Long itemCategory) {
        this.itemCategory = itemCategory;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
