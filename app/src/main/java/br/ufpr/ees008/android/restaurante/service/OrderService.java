package br.ufpr.ees008.android.restaurante.service;

import java.util.List;

import br.ufpr.ees008.android.restaurante.vo.AuthenticationVO;
import br.ufpr.ees008.android.restaurante.vo.ItemVO;
import br.ufpr.ees008.android.restaurante.vo.OrderItemVO;
import br.ufpr.ees008.android.restaurante.vo.OrderVO;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OrderService {
    @GET("/supplies/")
    Call<List<ItemVO>> listItems(@Header("Authorization") String token);

    @GET("/orders/")
    Call<List<OrderVO>> listOrders(@Header("Authorization") String token);

    @POST("/orders/")
    Call<OrderVO> createOrder(@Header("Authorization") String token);

    @GET("/orders/items/")
    Call<List<OrderItemVO>> listOrderItens(@Header("Authorization") String token, @Query("order") Long id);

    @FormUrlEncoded
    @POST("/orders/items/")
    Call<AuthenticationVO> addItem(
            @Header("Authorization") String token,
            @Field("item") Long itemId,
            @Field("amount") int amount,
            @Field("order") Long orderId
    );

    @FormUrlEncoded
    @POST("/login/")
    Call<AuthenticationVO> doLogin(@Field("username") String username, @Field("password") String password);

    @GET("/orders/{id}")
    Call<OrderVO> loadOrder(
            @Header("Authorization") String token,
            @Path("id") Long id
    );

    @FormUrlEncoded
    @PATCH("/orders/{id}")
    Call<Object> payOrder(
            @Header("Authorization") String token,
            @Path("id") Long id,
            @Field("is_paid") Boolean isPaid,
            @Field("payment_type") int paymentType
    );
}
