package br.ufpr.ees008.android.restaurante.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.ufpr.ees008.android.restaurante.R;
import br.ufpr.ees008.android.restaurante.vo.ItemVO;
import br.ufpr.ees008.android.restaurante.vo.OrderItemVO;

/**
 * Created by regis on 06/03/2016.
 */
public class OrderItemAdapter extends BaseAdapter {
    List<ItemVO> orderItemVOList;
    LayoutInflater layoutInflater;

    class ViewHolder {
        ImageView itemPicture;
        TextView itemName;
        TextView itemDescription;

    }

    public OrderItemAdapter(Context context, List<ItemVO> orderItemVOList) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.orderItemVOList = orderItemVOList;
    }

    @Override
    public int getCount() {
        return orderItemVOList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderItemVOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return orderItemVOList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_order_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.itemPicture = (ImageView) convertView.findViewById(R.id.itemIconImageView);
            viewHolder.itemName = (TextView) convertView.findViewById(R.id.itemNameTextView);
            viewHolder.itemDescription = (TextView) convertView.findViewById(R.id.itemDescriptionTextView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ItemVO item = this.orderItemVOList.get(position);

        byte[] decodedString = Base64.decode(item.getPicture().split("base64,")[1], Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        viewHolder.itemPicture.setImageBitmap(decodedByte);
        viewHolder.itemName.setText(item.getName());
        viewHolder.itemDescription.setText(String.format("R$ %.2f", item.getPrice()));

        return convertView;
    }
}
