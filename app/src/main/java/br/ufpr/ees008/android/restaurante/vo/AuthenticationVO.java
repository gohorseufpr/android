package br.ufpr.ees008.android.restaurante.vo;

public class AuthenticationVO {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
