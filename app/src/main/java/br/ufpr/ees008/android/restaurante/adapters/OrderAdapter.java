package br.ufpr.ees008.android.restaurante.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.ufpr.ees008.android.restaurante.vo.OrderItemVO;
import br.ufpr.ees008.android.restaurante.vo.OrderVO;

public class OrderAdapter extends BaseAdapter {
    List<OrderItemVO> orderItemVOList;
    LayoutInflater layoutInflater;

    class ViewHolder {
        TextView itemName;
        TextView itemDescription;

    }

    public OrderAdapter(Context context, List<OrderItemVO> orderItemVOList) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.orderItemVOList = orderItemVOList;
    }

    @Override
    public int getCount() {
        return orderItemVOList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderItemVOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return orderItemVOList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(android.R.layout.simple_list_item_2, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.itemName = (TextView) convertView.findViewById(android.R.id.text1);
            viewHolder.itemDescription = (TextView) convertView.findViewById(android.R.id.text2);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        OrderItemVO item = this.orderItemVOList.get(position);
        viewHolder.itemName.setText(String.valueOf(item.getAmount()) + "x " + item.getItem().getName());
        viewHolder.itemDescription.setText(String.format("R$ %.2f", item.getItem().getPrice()));

        return convertView;
    }


}
