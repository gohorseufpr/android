package br.ufpr.ees008.android.restaurante.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import br.ufpr.ees008.android.restaurante.R;
import br.ufpr.ees008.android.restaurante.RestauranteApplication;
import br.ufpr.ees008.android.restaurante.interectors.OrderInterector;
import br.ufpr.ees008.android.restaurante.preferences.AuthPreferences;
import br.ufpr.ees008.android.restaurante.service.OrderService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayActivity extends AppCompatActivity {

    OrderService orderService;
    AuthPreferences authPreferences;
    RadioGroup radioGroup;
    Button buttonCancel;
    Button buttonDoPay;
    Long orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        getSupportActionBar().setTitle("Pagar");
        orderId = getIntent().getLongExtra("order_id", 0);
        orderService = RestauranteApplication.getOrderService();
        authPreferences = RestauranteApplication.getAuthPreferences();
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        buttonCancel = (Button) findViewById(R.id.buttonCancelPay);
        buttonDoPay = (Button) findViewById(R.id.buttonDoPay);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonDoPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int paymentType = 0;
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case (R.id.radioButton):
                        paymentType = 1;
                        break;
                    case (R.id.radioButton2):
                        paymentType = 2;
                        break;
                    case (R.id.radioButton3):
                        paymentType = 3;
                        break;
                }
                if (paymentType == 0) {
                    Toast.makeText(PayActivity.this, "Selecione uma forma de pagamento.", Toast.LENGTH_LONG).show();
                } else {
                    orderService.payOrder(authPreferences.getToken(), orderId, true, paymentType).enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            if (response.isSuccessful()) {
                                final OrderInterector orderInterector = new OrderInterector(
                                        RestauranteApplication.getOrderService(), new AuthPreferences(PayActivity.this));
                                orderInterector.getCurrentOrder(new OrderInterector.Callback() {
                                    @Override
                                    public void onOrderFetch(Long id) {
                                        Intent intent = new Intent();
                                        intent.putExtra("order_id", id);
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    }

                                    @Override
                                    public void onOrderFetchError(String error) {
                                        Toast.makeText(PayActivity.this, error, Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onUnauthorized(String error) {
                                        authPreferences.logout();
                                        showLoginScreen();
                                        finish();
                                    }
                                });

                            } else {
                                if (response.code() == 401) {
                                    authPreferences.logout();
                                    showLoginScreen();
                                    finish();
                                }
                                Toast.makeText(PayActivity.this, "ERRO", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            Toast.makeText(PayActivity.this, "ERRO", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    public void showLoginScreen() {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }
}
