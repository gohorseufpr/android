package br.ufpr.ees008.android.restaurante;

import android.app.Application;
import android.content.Context;

import br.ufpr.ees008.android.restaurante.preferences.AuthPreferences;
import br.ufpr.ees008.android.restaurante.service.OrderService;
import br.ufpr.ees008.android.restaurante.service.RetrofitClient;

public class RestauranteApplication extends Application {

    static AuthPreferences authPreferences;
    static OrderService orderService;

    @Override
    public void onCreate() {
        super.onCreate();
        orderService = new RetrofitClient().create().create(OrderService.class);
        authPreferences = new AuthPreferences(this);
    }

    public static OrderService getOrderService() {
        return orderService;
    }

    public static AuthPreferences getAuthPreferences() {
        return authPreferences;
    }
}
