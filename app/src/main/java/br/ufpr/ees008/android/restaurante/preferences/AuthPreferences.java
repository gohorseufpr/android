package br.ufpr.ees008.android.restaurante.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class AuthPreferences {
    private static final String PREFERENCE_AUTH = "auth_preference";
    private SharedPreferences preferences;

    public AuthPreferences(Context context) {
        preferences = context.getSharedPreferences(PREFERENCE_AUTH, 0);
    }

    public void authenticate(String token) {
        preferences.edit().putString("token", "token " + token).apply();
    }


    public void logout() {
        preferences.edit().clear().apply();
    }

    public String getToken() {
        return preferences.getString("token", null);
    }

    public Boolean isAuthenticated() {
        return this.getToken() != null;
    }
}
