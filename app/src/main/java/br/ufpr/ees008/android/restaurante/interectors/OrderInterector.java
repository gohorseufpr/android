package br.ufpr.ees008.android.restaurante.interectors;

import java.util.List;

import br.ufpr.ees008.android.restaurante.preferences.AuthPreferences;
import br.ufpr.ees008.android.restaurante.service.OrderService;
import br.ufpr.ees008.android.restaurante.vo.OrderVO;
import retrofit2.Call;
import retrofit2.Response;

public class OrderInterector {

    private OrderService orderService;
    private AuthPreferences authPreferences;

    public OrderInterector(OrderService orderService, AuthPreferences authPreferences) {
        this.authPreferences = authPreferences;
        this.orderService = orderService;
    }

    public void getCurrentOrder(final Callback callback) {
        orderService.listOrders(authPreferences.getToken()).enqueue(new retrofit2.Callback<List<OrderVO>>() {
            @Override
            public void onResponse(Call<List<OrderVO>> call, Response<List<OrderVO>> response) {
                if (response.isSuccessful()) {
                    List<OrderVO> orders = response.body();
                    OrderVO currentOrder = null;
                    for (OrderVO order : orders) {
                        if (!order.getIsPaid()) {
                            currentOrder = order;
                        }
                    }
                    if (currentOrder == null) {
                        orderService.createOrder(authPreferences.getToken()).enqueue(new retrofit2.Callback<OrderVO>() {
                            @Override
                            public void onResponse(Call<OrderVO> call, Response<OrderVO> response) {
                                if (response.isSuccessful()) {
                                    callback.onOrderFetch(response.body().getId());
                                } else {
                                    if (response.code() == 401) {
                                        callback.onUnauthorized("Erro na requisição. Tente novamente mais tarde");
                                    } else {
                                        callback.onOrderFetchError("Erro na requisição. Tente novamente mais tarde");
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<OrderVO> call, Throwable t) {
                                callback.onOrderFetchError("Erro na requisição. Tente novamente mais tarde");
                            }
                        });
                    } else {
                        callback.onOrderFetch(currentOrder.getId());
                    }
                } else {
                    if (response.code() == 401) {
                        callback.onUnauthorized("Erro na requisição. Tente novamente mais tarde");
                    } else {
                        callback.onOrderFetchError("Erro na requisição. Tente novamente mais tarde");
                    }
                }
            }

            @Override
            public void onFailure(Call<List<OrderVO>> call, Throwable t) {
                callback.onOrderFetchError("Erro na requisição. Tente novamente mais tarde");
            }
        });

    }

    public interface Callback {
        void onOrderFetch(Long id);

        void onOrderFetchError(String error);

        void onUnauthorized(String error);
    }
}
