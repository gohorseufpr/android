package br.ufpr.ees008.android.restaurante.controller;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.ufpr.ees008.android.restaurante.R;
import br.ufpr.ees008.android.restaurante.RestauranteApplication;
import br.ufpr.ees008.android.restaurante.adapters.OrderAdapter;
import br.ufpr.ees008.android.restaurante.preferences.AuthPreferences;
import br.ufpr.ees008.android.restaurante.service.OrderService;

import br.ufpr.ees008.android.restaurante.vo.OrderItemVO;
import br.ufpr.ees008.android.restaurante.vo.OrderVO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {
    public static final int REQUEST_ADD_ITEM_ORDER = 1;
    public static final int REQUEST_PAY_ORDER = 2;

    OrderService orderService;
    AuthPreferences authPreferences;
    ListView orderListView;
    Button payButton;
    FloatingActionButton addButton;
    TextView emptyOrderText;
    TextView totalTextView;
    Long orderId;
    boolean shoudReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        getSupportActionBar().setTitle("Pedido");
        orderListView = (ListView) findViewById(R.id.listViewOrders);
        orderService = RestauranteApplication.getOrderService();
        authPreferences = RestauranteApplication.getAuthPreferences();
        emptyOrderText = (TextView) findViewById(R.id.textEmptyOrder);
        totalTextView = (TextView) findViewById(R.id.textViewTotal);
        if (savedInstanceState == null) {
            orderId = getIntent().getLongExtra("order_id", 0);
        } else {
            orderId = savedInstanceState.getLong("order_id", 0);
        }
        getOrderData();

        findViewById(R.id.buttonAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOrderItemScreen();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_menu, menu);
        Adapter adapter = orderListView.getAdapter();
        menu.findItem(R.id.actionMenuPay).setEnabled(adapter != null && !adapter.isEmpty());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionMenuPay:
                showPayScreen();
                return true;
            case R.id.actionMenuLogout:
                authPreferences.logout();
                showLoginScreen();
                finish();
                return true;
        }
        return false;
    }

    public void getOrderData() {
        orderService.listOrderItens(authPreferences.getToken(), orderId).enqueue(new Callback<List<OrderItemVO>>() {
            @Override
            public void onResponse(Call<List<OrderItemVO>> call, Response<List<OrderItemVO>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        emptyOrderText.setVisibility(View.GONE);
                    } else {
                        emptyOrderText.setVisibility(View.VISIBLE);
                    }
                    orderListView.setAdapter(new OrderAdapter(OrderActivity.this, response.body()));
                    invalidateOptionsMenu();
                } else {
                    Log.d("response", String.valueOf(response.code()));
                    if (response.code() == 401) {
                        authPreferences.logout();
                        showLoginScreen();
                        finish();
                    }
                    Toast.makeText(OrderActivity.this, "Tente novamente mais tarde.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<OrderItemVO>> call, Throwable t) {
                t.printStackTrace();
                Log.d("response", "PAU");
                Toast.makeText(OrderActivity.this, "Tente novamente mais tarde.", Toast.LENGTH_LONG).show();
            }
        });
        orderService.loadOrder(authPreferences.getToken(), orderId).enqueue(new Callback<OrderVO>() {
            @Override
            public void onResponse(Call<OrderVO> call, Response<OrderVO> response) {
                if (response.isSuccessful()) {
                    totalTextView.setText(String.format("Total: R$ %.2f", response.body().getTotal()));
                }
            }

            @Override
            public void onFailure(Call<OrderVO> call, Throwable t) {

            }
        });
    }

    public void showPayScreen() {
        Intent intent = new Intent(this, PayActivity.class);
        intent.putExtra("order_id", orderId);
        startActivityForResult(intent, REQUEST_PAY_ORDER);
    }

    public void showOrderItemScreen() {
        Intent intent = new Intent(this, OrderItemActivity.class);
        intent.putExtra("order_id", orderId);
        startActivityForResult(intent, REQUEST_ADD_ITEM_ORDER);
    }

    public void showLoginScreen() {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_ITEM_ORDER && resultCode == RESULT_OK) {
            shoudReload = true;
        } else if (requestCode == REQUEST_PAY_ORDER && resultCode == RESULT_OK) {
            orderId = data.getLongExtra("order_id", 0);
            shoudReload = true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("order_id", orderId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (shoudReload) {
            shoudReload = false;
            getOrderData();
        }
    }
}
