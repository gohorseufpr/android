package br.ufpr.ees008.android.restaurante.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.ufpr.ees008.android.restaurante.R;
import br.ufpr.ees008.android.restaurante.RestauranteApplication;
import br.ufpr.ees008.android.restaurante.interectors.OrderInterector;
import br.ufpr.ees008.android.restaurante.preferences.AuthPreferences;
import br.ufpr.ees008.android.restaurante.vo.AuthenticationVO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenticationActivity extends AppCompatActivity {
    AuthPreferences authPreferences;
    Button loginButton;
    EditText usernameEditText;
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authPreferences = RestauranteApplication.getAuthPreferences();
        if (authPreferences.isAuthenticated()) {
            loadCurrentOrder();
            return;
        }

        setContentView(R.layout.activity_authentication);
        loginButton = (Button) findViewById(R.id.buttonEnter);
        usernameEditText = (EditText) findViewById(R.id.editTextUsername);
        passwordEditText = (EditText) findViewById(R.id.editTextPassword);
        authPreferences = new AuthPreferences(this);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RestauranteApplication.getOrderService().doLogin(
                        usernameEditText.getText().toString(),
                        passwordEditText.getText().toString()
                ).enqueue(new Callback<AuthenticationVO>() {
                    @Override
                    public void onResponse(Call<AuthenticationVO> call, Response<AuthenticationVO> response) {
                        if (response.isSuccessful()) {
                            authPreferences.authenticate(response.body().getToken());
                            loadCurrentOrder();
                        } else {
                            Toast.makeText(AuthenticationActivity.this, "Usuário e Senha inválidos", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AuthenticationVO> call, Throwable t) {
                        Toast.makeText(AuthenticationActivity.this, "Erro na requisição, tente novamente mais tarde.", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void loadCurrentOrder() {
        final OrderInterector orderInterector = new OrderInterector(
                RestauranteApplication.getOrderService(), new AuthPreferences(this));
        orderInterector.getCurrentOrder(new OrderInterector.Callback() {
            @Override
            public void onOrderFetch(Long id) {
                showOrderScreen(id);
            }

            @Override
            public void onOrderFetchError(String error) {
                Toast.makeText(AuthenticationActivity.this, error, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUnauthorized(String error) {
                authPreferences.logout();
                showLoginScreen();
                finish();
            }
        });

    }

    public void showLoginScreen() {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }

    private void showOrderScreen(Long id) {
        Intent intent = new Intent(this, OrderActivity.class);
        intent.putExtra("order_id", id);
        startActivity(intent);
        finish();
    }
}
